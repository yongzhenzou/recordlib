package com.example.recordlib

import android.app.Service
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Vibrator
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.permissionx.guolindev.PermissionX
import com.permissionx.guolindev.callback.RequestCallback
import zyz.hero.record_kit.view.RecordLayout
import zyz.hero.record_kit.RecordState

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var textView = findViewById<TextView>(R.id.touchView)
        var recordLayout = findViewById<RecordLayout>(R.id.recordLayout)
        recordLayout.setRecordCallBack { recordFile, recordTimeMillis ->
            //
            if (recordTimeMillis < 1000) {
                if (recordFile?.exists() == true) {
                    recordFile?.delete()
                }
                Toast.makeText(this, "说话时间太短", Toast.LENGTH_SHORT).show()
            } else {

            }
        }
        textView.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkPermissions()) {
                            vibrate()
                            recordLayout.visibility = View.VISIBLE
                            recordLayout.setFile(FileUtils.getVoiceFile(this@MainActivity))
                            recordLayout.recordState = RecordState.START
                        }
                    }else{
                        vibrate()
                        recordLayout.visibility = View.VISIBLE
                        recordLayout.setFile(FileUtils.getVoiceFile(this@MainActivity))
                        recordLayout.recordState = RecordState.START
                    }
                }
                MotionEvent.ACTION_MOVE -> {
                    if (event.y < -10.dp) {
                        recordLayout.recordState = RecordState.CANCEL
                    } else {
                        recordLayout.recordState = RecordState.RECORDING
                    }
                }
                MotionEvent.ACTION_CANCEL -> {
                    recordLayout.recordState = RecordState.RELEASE
                    recordLayout.visibility = View.GONE
                }
                MotionEvent.ACTION_UP -> {
                    recordLayout.recordState = RecordState.RELEASE
                    recordLayout.visibility = View.GONE
                }
                else -> {}
            }
            return@setOnTouchListener true
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun checkPermissions(): Boolean {
        if (PackageManager.PERMISSION_GRANTED != checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) ||
            PackageManager.PERMISSION_GRANTED != checkSelfPermission(android.Manifest.permission.RECORD_AUDIO)
        ) {
            requestPermissions(arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                android.Manifest.permission.RECORD_AUDIO), 123)
            return false
        }
        return true
    }

    fun vibrate() {
        var vibrator = getSystemService(Service.VIBRATOR_SERVICE) as? Vibrator
        vibrator?.vibrate(50)
    }
}